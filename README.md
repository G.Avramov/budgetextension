**This is a Browser Action Extension that uses the Chrome API's.**

It has the following options:
- accepts spendings
- displays total spendings
- has a limit and a notification if it's reached
- reset "Total" or reset "Limit" and a notification when a change occures



**Right click on the icon and press "options" to get to the Options Page:**


![Options](/uploads/8038da4b37f803ada65a10494381c167/Options.png)

![OptionsPage](/uploads/99cf2e0a5895dfa760b478e78c8c170d/OptionsPage.png)



**When limit is reached:**

![LimitReachedNotif](/uploads/f4415e05542535616830219ac563146d/LimitReachedNotif.png)



**When total is reset:**

![ResetTotalNotif](/uploads/d7d9e30649f0701354516f07e8cd7a42/ResetTotalNotif.png)



**There is a context menu function for spending money(adds a number to the total) by right-clicking on a number form a web page and selecting the "SpendMoney" button:**

![SpendMoney](/uploads/62d0346151e337457002a0fa0c84d0bc/SpendMoney.png)
